(*
	Trabalho 1+2 - L1 sem/com recursao
	Data de entrega: 07/06/2015
	Grupo: Andrey Blazejuk, Luiza Hagemann
*)

(*
	Grammar:
	e ::= 
		n 
		| b 
		| e1 op e2 
		| if e1 then e2 else e3
		| x 
		| e1 e2 
		| fn x ⇒ e 
		| let x = e1 in e2
*)

        module EnvironmentMap = Map.Make(String);;

	type variable = string;;
	type operator =
	    Sum 
	  | Diff 
	  | Mult
	  | Div
	  | Equal
	  | Lessorequal
	  ;;
		
	type expression =
	    Num of int 
	  | Bool of bool 
	  | BinOp of expression * operator * expression
	  | If of expression * expression * expression 
	  | Var of variable 
	  | Applic of expression * expression
	  | Function of variable * expression 
	  | Let of variable * expression * expression
	  | LetRec of variable * expression * expression
	  ;;
	
	type value = 
	    VNum of int 
	  | VBool of bool 
	  | VClosure of variable * expression * environment
	and
	    environment = expression EnvironmentMap.t;;

(* environment control	
   empty *)
let environment = EnvironmentMap.empty
 
(* add *)
let add_to_environment (x:string) (e:expression) =
    EnvironmentMap.add x e environment

(* lookup *)
let lookup_environment (x:string) =
   EnvironmentMap.find x environment
        failwith "Not_found"

(* update *)
let update_environment (x:string) (e:expression) = 
  EnvironmentMap.add x e environment


(* EVALUATION *)  
  
let rec eval (environment:environment) (e:expression) : value = match e with
(* VALUES *)
  Num(n) -> VNum(n)
| Bool(b) -> VBool(b)

(* OPERATIONS:           
   
   Sum *)

| BinOp(e1, Sum, e2) -> let VNum(n1) = eval environment e1 in
  let VNum(n2) = eval environment e2 in
  VNum(n1 + n2)

(* Diff 
   implemented with integer logic, so it is equivalent to it's sum counterpart *)

| BinOp(e1, Diff, e2) -> let VNum(n1) = eval environment e1 in
  let VNum(n2) = eval environment e2 in
  VNum(n1 - n2)

(* Mult *)

| BinOp(e1, Mult, e2) -> let VNum(n1) = eval environment e1 in
  let VNum(n2) = eval environment e2 in
  VNum(n1 * n2)

(* Div 
   n1/0 cannot happen, but ocaml treats the exception on its own *)

| BinOp(e1, Div, e2) -> let VNum(n1) = eval environment e1 in
  let VNum(n2) = eval environment e2 in
  VNum(n1 / n2)

(* Equal *)

| BinOp(e1, Equal, e2) -> let VNum(n1) = eval environment e1 in
  let VNum(n2) = eval environment e2 in
  VBool(n1 = n2)
| BinOp(e1, Equal, e2) -> let VBool(b1) = eval environment e1 in
  let VBool(b2) = eval environment e2 in
  VBool(b1 = b2)

(* Lessorequal *)

| BinOp(e1, Lessorequal, e2) -> let VNum(n1) = eval environment e1 in
  let VNum(n2) = eval environment e2 in
  VBool(n1 <= n2)


(* IF *)

| If(e1,e2,e3) when ((eval environment e1) = VBool(true)) -> eval environment e2
| If(e1,e2,e3) when ((eval environment e1) = VBool(false)) -> eval environment e3

(* VARIABLE  *)

| Var(variable) -> lookup_environment variable


(* FUNCTION  *)

| Function(variable,e) -> VClosure(variable, e, environment)


(* APPLICATION *)

| Applic(e1, e2) when ((eval environment e1) = VClosure(var,expr,environment) -> let v2 = eval environment e2 in
 (eval (add_to_environment var v2) (substitute v2 var expr))

(* LET *)


(* LETREC *)

(* else (everything else fails) *)

| _ -> failwith "unimplemented"

;;


(* para fazer application, let e letrec, precisamos utilizar a regra de substituicao lambda.
   a regra diz que:
   i) var:
   x [x <- X] = X
   y [x <- X] = y

   ii)applic
   (e1 e2) [x <- X] = ((e1 [x <- X]) (e2 [x <- X]))

   iii) abstracoes lambda:
   1) lx.M [x <- X] = lx.M (so eh possivel substituir quando eh livre)
   2) ly.M [x <- X] = ly.(M [x <- X]) (se y nao ocorre livre em x e x nao ocorre livre em M)
   3) ly.M [x <- X] = lz.((M [y <- z])[x <- X]) (se y ocorre livre em X e x ocorre livre em M e z nao ocorre livre em nenhuma das duas)

*)

(* SUBSTITUTION (for applic and lets) *)
let rec substitute value identifier expr = match expr with

  Num(n) -> expr
| Bool(b) -> expr

| BinOp(e1, op, e2) -> BinOp((substitute value identifier e1), op, (substitute value identifier e2))

| Var(x) when (identifier = x) -> value
| Var(x) when (identifier <> x) -> Var(x)

| If(e1, e2, e3) -> If((substitute value identifier e1),(substitute value identifier e2),(substitute value identifier e3))

| Applic(e1, e2) -> Applic((substitute value identifier e1), (substitute value identifier e2))

| Function(x,e) when (identifier = x) -> Function(x,e)
| Function(y,e) when (identifier <> y) -> (* rule 3 of lamda abstraction substitution *)
    let used_terms = List.append (find_fvs e) [identifier;y] in
    let z = create_new_term used_terms y in
    Function(z,(substitute value identifier (substitute z y e)))

(* colocar let e letrec depois *) 

;;


(* FIND FREE VARIABLES IN EXPRESSION E *)
let rec find_fvs (e:expression) = match e with

  Num(n) -> []
| Bool(b) -> []
| BinOp(e1,op,e2) -> List.append (find_fvs e1) (find_fvs e2) (* acha fvs em e1 e e2 e concatena tudo *)
| Var(x) -> [x]
| If(e1,e2,e3) -> List.append (find_fvs e1) (List.append (find_fvs e2) (find_fvs e3))
| Function(var,e) -> filter_out var (find_fvs e)
| Applic(e1,e2) -> List.append (find_fvs e1) (find_fvs e2)
(* fazer let e letrec depois *)

;;


(* FILTER ELEMENT OUT OF A LIST *)

let filter_out element list = List.filter (fun x -> x <> element) list ;;


(* CREATES A VAR NAME NOT USED BEFORE *)

let rec create_new_string used_variables var count = match used_variables with
  [] -> var^(string_of_int count)
| head::tail -> 
    if (head == var^(string_of_int count)) 
    then create_new_string tail var (count+1) 
    else create_new_var tail var count
;;


(* TESTS *)

(* simple language values *)

let numPass = Num(10);;
(* let numFail = Num(true);; ocaml doesnt let it pass *)
let boolPass = Bool(true);;
(* let boolFail = Bool(2);; ocaml doesnt let it pass *)


(* map structure for environment *)

add_to_environment "numPass" numPass;;
lookup_environment "numPass";;
lookup_environment "numFail";;
let num = Num 2;;
update_environment "numPass" num;;


(* binary operators *)

let sumPass = BinOp(Num(1), Sum, Num(1));;
let sumFail = BinOp(Num(1), Sum, Bool(true));;
let diffPass = BinOp(Num(2), Diff, Num(1));;
let diffFail = BinOp(Bool(false), Diff, Num(1));;
let multPass = BinOp(Num(2), Mult, Num(2));;
let multFail = BinOp(Num(2), Mult, Bool(true));;
let divPass = BinOp(Num(4), Div, Num(2));;
let divFail = BinOp(Bool(false), Div, Num(2));;
let divRaise = BinOp(Num(3), Div, Num(0));;
let equalNumTruePass = BinOp(Num(2), Equal, Num(2));;
let equalNumFalsePass = BinOp(Num(1), Equal, Num(2));;
let equalNumFail = BinOp(Num(1), Equal, Bool(true));;
let leOrEqTruePass = BinOp(Num(1), Lessorequal, Num(2));;
let leOrEqFalsePass = BinOp(Num(2), Lessorequal, Num(1));;
let leOrEqFail = BinOp(Num(1), Lessorequal, Bool(false));;


(* if *)

let ifTruePass = If(BinOp(Num(1), Equal, Num(1)), Bool(true), Bool(false));;
let ifFalsePass = If(BinOp(Num(2), Equal, Num(1)), Bool(true), Bool(false));;
let ifFail = If(BinOp(Num(1), Sum, Num(1)), Num(2), Num(3));;


(* EVAL *)

(* binary operations *)

let evalSumPass = eval environment sumPass;;
let evalSumFail = eval environment sumFail;;
let evalDiffPass = eval environment diffPass;;
let evalDiffFail = eval environment diffFail;;
let evalMultPass = eval environment multPass;;
let evalMultFail = eval environment multFail;;
let evalDivPass = eval environment divPass;;
let evalDivFail = eval environment divFail;;
let evalDivRaise = eval environment divRaise;;
let evalEqualNumTruePass = eval environment equalNumTruePass;;
let evalEqualNumFalsePass = eval environment equalNumFalsePass;;
let evalEqualNumFail = eval environment equalNumFail;;
let evalLeOrEqTruePass = eval environment leOrEqTruePass;;
let evalLeOrEqFalsePass = eval environment leOrEqFalsePass;;
let evalLeOrEqFail = eval environment leOrEqFail;;

(* if *)

let evalIfTruePass = eval environment ifTruePass;;
let evalIfFalsePass = eval environment ifFalsePass;;
let evalIfFail = eval environment ifFail;;
